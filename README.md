# Hackernews Top Stories

vanilla js and async generators

## Run The Project
I've used [Parcel](https://parceljs.org/) for setting up the build system quickly 
Dev Server:

```
npm start
```
Prod:

```
npm build -- --public-url "$path"
```
[Jest](https://jestjs.io/) testing:

```
npm test
```


